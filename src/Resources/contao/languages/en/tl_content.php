<?php

/**
 * 361GRAD Element List
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_list']     = ['Aufzählung', 'Einzelner Listenpunkt.'];

$GLOBALS['TL_LANG']['tl_content']['secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['dse_listheadline'] = ['Listenpunkt-Überschrift', 'Überschrift für Listenpunkt'];
$GLOBALS['TL_LANG']['tl_content']['dse_listtext']     = ['Listenpunkt-Text', 'Text unter Überschrift in Listenpunkt'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];